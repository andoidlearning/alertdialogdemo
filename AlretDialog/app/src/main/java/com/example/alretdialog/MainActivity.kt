package com.example.alretdialog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn1 : Button = findViewById(R.id.btnDialog1)
        val btn2 : Button = findViewById(R.id.btnDialog2)
        val btn3 : Button = findViewById(R.id.btnDialog3)

        val addContactDialog = AlertDialog.Builder(this)
            .setTitle("Add contact")
            .setMessage("Do you want to add Mr. Poop to your contact")
            .setIcon(R.drawable.ic_add_contact)
            .setPositiveButton("Yes"){ _, _->
                Toast.makeText(this,"You added Mr. Poop to your contact list",Toast.LENGTH_SHORT).show()
            }
            .setNegativeButton("No"){ _, _->
                Toast.makeText(this,"You didn't add Mr. Poop to your contact list",Toast.LENGTH_SHORT).show()
            }.create()

        btn1.setOnClickListener {
            addContactDialog.show()
        }

        val options = arrayOf("First Item", "Second Item", "Third Item")

        val singleChoiceDialog = AlertDialog.Builder(this)
            .setTitle("Choose one of this options")
            .setSingleChoiceItems(options,0){dialogInterface, i->
                Toast.makeText(this,"You clicked on ${options[i]}",Toast.LENGTH_SHORT).show()
            }
            .setPositiveButton("Accept"){ _, _->
                Toast.makeText(this,"You accepted the SingleChoiceDialog",Toast.LENGTH_SHORT).show()
            }
            .setNegativeButton("Decline"){ _, _->
                Toast.makeText(this,"You decline the SingleChoiceDialog",Toast.LENGTH_SHORT).show()
            }.create()

        btn2.setOnClickListener {
            singleChoiceDialog.show()
        }

        val multiChoiceDialog = AlertDialog.Builder(this)
            .setTitle("Choose one of this options")
            .setMultiChoiceItems(options, booleanArrayOf(false,false,false)){_, i, isChecked ->
                if(isChecked) {
                    Toast.makeText(this, "You checked on ${options[i]}", Toast.LENGTH_SHORT).show()
                }
                else{
                    Toast.makeText(this, "You unchecked on ${options[i]}", Toast.LENGTH_SHORT).show()
                }
            }
            .setPositiveButton("Accept"){ _, _->
                Toast.makeText(this,"You accepted the MultiChoiceDialog",Toast.LENGTH_SHORT).show()
            }
            .setNegativeButton("Decline"){ _, _->
                Toast.makeText(this,"You decline the MultiChoiceDialog",Toast.LENGTH_SHORT).show()
            }.create()

        btn3.setOnClickListener {
            multiChoiceDialog.show()
        }
    }
}